package drink.web;

import drink.domain.Plan;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/plans")
@Controller
@RooWebScaffold(path = "plans", formBackingObject = Plan.class)
public class PlanController {
}
