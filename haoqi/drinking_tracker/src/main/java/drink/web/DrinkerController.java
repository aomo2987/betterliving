package drink.web;

import drink.domain.Drinker;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/drinkers")
@Controller
@RooWebScaffold(path = "drinkers", formBackingObject = Drinker.class, delete=false)
public class DrinkerController {
}
