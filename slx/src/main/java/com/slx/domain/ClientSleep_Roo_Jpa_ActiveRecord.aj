// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.slx.domain;

import com.slx.domain.ClientSleep;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect ClientSleep_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager ClientSleep.entityManager;
    
    public static final EntityManager ClientSleep.entityManager() {
        EntityManager em = new ClientSleep().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long ClientSleep.countClientSleeps() {
        return entityManager().createQuery("SELECT COUNT(o) FROM ClientSleep o", Long.class).getSingleResult();
    }
    
    public static List<ClientSleep> ClientSleep.findAllClientSleeps() {
        return entityManager().createQuery("SELECT o FROM ClientSleep o", ClientSleep.class).getResultList();
    }
    
    public static ClientSleep ClientSleep.findClientSleep(Long id) {
        if (id == null) return null;
        return entityManager().find(ClientSleep.class, id);
    }
    
    public static List<ClientSleep> ClientSleep.findClientSleepEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM ClientSleep o", ClientSleep.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void ClientSleep.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void ClientSleep.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            ClientSleep attached = ClientSleep.findClientSleep(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void ClientSleep.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void ClientSleep.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public ClientSleep ClientSleep.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        ClientSleep merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
