package com.slx.domain;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(identifierColumn = "id", identifierField = "id")
public class ClientSleep {

    @NotNull
    @OneToOne
    private Client client;

    @NotNull
    @DecimalMin("4")
    @DecimalMax("16")
    private BigDecimal sleep_target;

    @NotNull
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Sleep> sleep_events = new HashSet<Sleep>();
}
