package com.slx.domain;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(identifierColumn = "id", identifierField = "id")
public class Sleep {

    @NotNull
    @ManyToOne
    private Client client;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date sleep_date;
	
    @NotNull
    @DecimalMin("0")
    @DecimalMax("24")
    private BigDecimal hours_slept;

    @NotNull
    @DecimalMin("0")
    @DecimalMax("5")
    private BigDecimal sleep_rating;

    private String sleep_comment;


}
