package com.slx.domain;

import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(identifierColumn = "id", identifierField = "id")
public class Location {

    @NotNull
    @OneToOne
    private Client client;

    @NotNull
    @Enumerated
    private Countries country;

    @NotNull
    @Min(0L)
    @Max(9999L)
    private int postcode;

    @Min(0L)
    private double latitude;

    @Min(0L)
    private double longitude;
}
