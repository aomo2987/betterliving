package com.slx.domain;

import java.math.BigDecimal;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(identifierColumn = "id", identifierField = "id")
public class Profile {

    @NotNull
    @OneToOne
    private Client client;

    @NotNull
    @Size(min = 2, max = 32)
    private String first_name;

    @NotNull
    @Size(min = 2, max = 32)
    private String last_name;

    @NotNull
    @Enumerated
    private Gender gender;

    @Pattern(regexp = "[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+")
    private String email;

    @Min(10L)
    @Max(120L)
    private int age;

    @NotNull
    @DecimalMin("20")
    @DecimalMax("650")
    private BigDecimal weight;

    @NotNull
    @DecimalMin("20")
    @DecimalMax("300")
    private BigDecimal height;
}
