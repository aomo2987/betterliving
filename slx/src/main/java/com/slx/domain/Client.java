package com.slx.domain;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(identifierColumn = "id", identifierField = "id")
public class Client {

    @NotNull
    @Column(unique = true)
    @Size(min = 6)
    private String username;

    @NotNull
    @Size(min = 6, max = 32)
    private String password;
}
