package com.slx.web;

import com.slx.domain.Sleep;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/sleeps")
@Controller
@RooWebScaffold(path = "sleeps", formBackingObject = Sleep.class)
public class SleepController {
}
