package com.slx.web;

import com.slx.domain.ClientSleep;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/clientsleeps")
@Controller
@RooWebScaffold(path = "clientsleeps", formBackingObject = ClientSleep.class)
public class ClientSleepController {
}
