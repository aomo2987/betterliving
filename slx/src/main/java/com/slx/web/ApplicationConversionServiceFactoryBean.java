package com.slx.web;

import com.slx.domain.Client;
import com.slx.domain.ClientSleep;
import com.slx.domain.Location;
import com.slx.domain.Profile;
import com.slx.domain.Sleep;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.roo.addon.web.mvc.controller.converter.RooConversionService;

/**
 * A central place to register application converters and formatters. 
 */
@RooConversionService
public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {

	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
	}

	public Converter<ClientSleep, String> getClientSleepToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.slx.domain.ClientSleep, java.lang.String>() {
            public String convert(ClientSleep clientSleep) {
                return new StringBuilder().append(clientSleep.getClient().getUsername()).append(' ').append(clientSleep.getSleep_target()).toString();
            }
        };
    }

	public Converter<Client, String> getClientToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.slx.domain.Client, java.lang.String>() {
            public String convert(Client client) {
                return client.getUsername();
            }
        };
    }

	public Converter<Location, String> getLocationToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.slx.domain.Location, java.lang.String>() {
            public String convert(Location location) {
                return new StringBuilder().append(location.getClient().getUsername()).append(' ').append(location.getCountry()).append(' ').append(location.getPostcode()).append(' ').append(location.getLatitude()).append(' ').append(location.getLongitude()).toString();
            }
        };
    }

	public Converter<Profile, String> getProfileToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.slx.domain.Profile, java.lang.String>() {
            public String convert(Profile profile) {
                return new StringBuilder().append(profile.getClient().getUsername()).append(' ').append(profile.getFirst_name()).append(' ').append(profile.getLast_name()).append(' ').append(profile.getEmail()).append(' ').append(profile.getAge()).toString();
            }
        };
    }

	public Converter<Sleep, String> getSleepToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.slx.domain.Sleep, java.lang.String>() {
            public String convert(Sleep sleep) {
                return new StringBuilder().append(sleep.getClient().getUsername()).append(' ').append(sleep.getSleep_date()).append(' ').append(sleep.getHours_slept()).append(' ').append(sleep.getSleep_rating()).append(' ').append(sleep.getSleep_comment()).toString();
            }
        };
    }
}
